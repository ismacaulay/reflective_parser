package com.imacaulay;

import com.imacaulay.FatalErrorException.ErrorCode;

import java.net.URLClassLoader;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.File;

/**
 * A class used to load a jar file
 */
public class JarFileLoader
{
   public JarFileLoader()
   {
   }
   
   /**
    * Loads the specified class from the given jar file
    *
    * @param jarFilePath The path to the jarfile
    * @param className The class to load from the jar file
    * @return A JarObject that represents the loaded class
    * @throws FatalErrorException If jar file is not found, or the class is invalid an exception is thrown
    */
   public JarObject loadJarFile(String jarFilePath, String className) throws FatalErrorException
   {
      Class jarClass = null;
      try
      {
         File file = new File(jarFilePath);
         if(!file.exists())
         {
            throw new FatalErrorException(ErrorCode.COULD_NOT_LOAD_JAR, jarFilePath);
         }

         URL[] urls = {file.toURI().toURL()};
         URLClassLoader classLoader = new URLClassLoader(urls, this.getClass().getClassLoader());
         jarClass = Class.forName(className, true, classLoader);
      } 
      catch (MalformedURLException e)
      {
         throw new FatalErrorException(ErrorCode.COULD_NOT_LOAD_JAR, jarFilePath);
      }
      catch (ClassNotFoundException e)
      {
         throw new FatalErrorException(ErrorCode.COULD_NOT_FIND_CLASS, className);
      }
      
      return new JarObject(jarClass);
   }
}

