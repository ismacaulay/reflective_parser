package com.imacaulay;

/**
 * Constants for the MetaCommands
 */
public class MetaCommandDefs
{
   public enum MetaCommand
   {
      NONE,
      QUIT,
      VERBOSE,
      LIST_METHODS,
      PRINT_HELP
   }
   
   /**
    * Restrict instantiation
    */
   private MetaCommandDefs()
   {
   }
}

