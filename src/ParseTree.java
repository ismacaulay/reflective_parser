package com.imacaulay;

/**
 * Represents a parse tree that contains a root Node object
 */
public class ParseTree
{
   private Node root_;

   public ParseTree(Node root)
   {
      root_ = root;
   }

   public boolean isEmpty()
   {
      return (root_ == null);
   }

   public Node root()
   {
      return root_;
   }

   public String toString()
   {
      if(root_ == null)
      {
         return "";
      }

      return root_.toString();
   }
}

