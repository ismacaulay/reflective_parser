package com.imacaulay;

/**
 * Contains constant strings used for output
 */
public final class OutputDefs
{
   public static final String SYNOPSIS = "Synopsis:\n" +
                                          "  methods\n" +
                                          "  methods { -h | -? | --help }+\n" + 
                                          "  methods {-v --verbose}* <jar-file> [<class-name>]\n" +
                                          "Arguments:\n" +
                                          "  <jar-file>:   The .jar file that contains the class to load (see next line).\n" +
                                          "  <class-name>: The fully qualified class name containing public static command methods to call. [Default=\"Commands\"]\n" +
                                          "Qualifiers:\n" +
                                          "  -v --verbose: Print out detailed errors, warning, and tracking.\n" +
                                          "  -h -? --help: Print out a detailed help message.\n" +
                                          "Single-char qualifiers may be grouped; long qualifiers may be truncated to unique prefixes and are not case sensitive.";

   public static final String DESCRIPTION = "This program interprets commands of the format '(<method> {arg}*)' on the command line, finds corresponding\n" +
                                             "methods in <class-name>, and executes them, printing the result to sysout.";

   public static final String INTERPRETER_HELP = "q           : Quit the program.\n" +
                                                 "v           : Toggle verbose mode (stack traces).\n" +
                                                 "f           : List all known functions.\n" +
                                                 "?           : Print this helpful text.\n" +
                                                 "<expression>: Evaluate the expression.\n" +
                                                 "Expressions can be integers, floats, strings (surrounded in double quotes) or function\n" +
                                                 "calls of the form '(identifier {expression}*)'.";

   /**
    * Should not be instantiated
    */
   private OutputDefs()
   {
   }
}

