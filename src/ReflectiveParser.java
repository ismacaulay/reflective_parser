package com.imacaulay;

public class ReflectiveParser
{
   public static void main(String[] args)
   {
      try
      {
         // parse the command line args
         CommandLineParser parser = new CommandLineParser();
         parser.parseArgs(args);

         // load the jar file
         JarFileLoader loader = new JarFileLoader();
         JarObject jarObject = loader.loadJarFile(parser.getJarFile(), parser.getClassName());

         // run the interpreter
         Interpreter interpreter = new Interpreter();
         interpreter.run(jarObject, parser.getVerbose());
      }
      catch(FatalErrorException e)
      {
         System.err.println(e);
         System.exit(e.exitCode());
      }  
   }

}
