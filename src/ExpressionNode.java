package com.imacaulay;

import java.util.ArrayList;

/**
 * A node that contains an expression which has paramters (children) associated with it
 */
public class ExpressionNode extends Node
{
   private String methodName_;
   private ArrayList<Node> children_;

   public ExpressionNode(String methodName, int offset)
   {
      super(offset);
      
      methodName_ = methodName;
      children_ = new ArrayList<Node>();
   }

   public void addChild(Node child)
   {
      children_.add(child);
   }

   public String toString()
   {
      String s = "(" + methodName_;
      for(Node n : children_)
      {
         s += " " + n.toString();
      }
      s += ")";

      return s;
   }

   public ArrayList<Node> children()
   {
      return children_;
   }

   public String methodName()
   {
      return methodName_;
   }
}

