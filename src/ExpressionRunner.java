package com.imacaulay;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Handles running of the expression if it is valid
 */
public class ExpressionRunner
{
   private JarObject jarObject_;

   public ExpressionRunner(JarObject jarObject)
   {
      jarObject_ = jarObject;
   }

   /**
    * Runs the commands specified in the parse tree
    *
    * @param parseTree The parse tree to execute
    * @throws ParseException If the parsing of the parse tree fails
    */
   public void execute(ParseTree parseTree) throws ParseException
   {
      if(parseTree.isEmpty())
      {
         return;
      }
      
      Node root = parseTree.root();

      // just output root if it is a integer, float, or string since it wont have children.
      if(root instanceof ValueNode)
      {
         System.out.println(root);
         return;
      }

      // root is a expression so execute it
      System.out.println(execute((ExpressionNode)root));
      return;
   }

   private Node execute(ExpressionNode node) throws ParseException
   {
      ArrayList<ValueNode> params = new ArrayList<ValueNode>();
      // traverse all of the children of the the node
      for(Node n : node.children())
      {
         Node param = n;
         // if the node is another expressions, recurse!
         if(n instanceof ExpressionNode)
         {
            param = execute((ExpressionNode)n);
         }

         // if the node is a value, add it to the list of params
         params.add((ValueNode)param);
      }
      
      // tell the jar object to invoke the method defined by the expression
      return jarObject_.invokeMethod(node, params);
   }
}

