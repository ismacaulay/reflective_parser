package com.imacaulay;

/**
 * Used to indicate a Fatal Error has occured
 */
public class FatalErrorException extends Exception
{
   public enum ErrorCode
   {
      UNRECOGNIZED_QUALIFIER,
      UNRECOGNIZED_LONG_QUALIFIER,
      TOO_MANY_ARGUMENTS,
      JAR_FILE_NOT_FIRST_ARG,
      HELP_HAS_EXTRA_QUALIFIERS,
      COULD_NOT_LOAD_JAR,
      COULD_NOT_FIND_CLASS
   }

   private String message_;
   private int exitCode_;

   public FatalErrorException(ErrorCode errorCode)
   {
      initialize(errorCode, "");
   }

   public FatalErrorException(ErrorCode errorCode, String arg)
   {
      initialize(errorCode, arg);
   }

   public int exitCode()
   {
      return exitCode_;
   }

   public String toString()
   {
      return message_;
   }

   /**
    * Intialized the exception with the correct message and exit code
    *
    * @param   errorCode   The errorCode that occurred
    * @param   arg         The argument that caused the error
    */
   private void initialize(ErrorCode errorCode, String arg)
   {
      switch(errorCode)
      {
         case UNRECOGNIZED_QUALIFIER:
         {
            char letter = invalidCharInQualifier(arg);
            message_ = "Unrecognized qualifier '" + letter + "' in '" + arg + "'.";
            exitCode_ = -1;
            break;
         }
         case UNRECOGNIZED_LONG_QUALIFIER:
         {
            message_ = "Unrecognized qualifier: " + arg + ".";
            exitCode_ = -1;
            break;
         }
         case TOO_MANY_ARGUMENTS:
         {
            message_ = "This program takes at most two command line arguments.";
            exitCode_ = -2;
            break;
         }
         case JAR_FILE_NOT_FIRST_ARG:
         {
            message_ = "This program requires a jar file as the first command line argument (after any qualifiers).";
            exitCode_ = -3;
            break;
         }
         case HELP_HAS_EXTRA_QUALIFIERS:
         {
            message_ = "Qualifier --help (-h, -?) should not appear with any comand-line arguments.";
            exitCode_ = -4;
            break;
         }
         case COULD_NOT_LOAD_JAR:
         {
            message_ = "Could not load jar file: " + arg;
            exitCode_ = -5;
            break;
         }
         case COULD_NOT_FIND_CLASS:
         {
            message_ = "Could not find class: " + arg;
            exitCode_ = -6;
            break;
         }
      }

      if(errorCode != ErrorCode.COULD_NOT_LOAD_JAR && errorCode != ErrorCode.COULD_NOT_FIND_CLASS)
      {
         message_ += "\n" + OutputDefs.SYNOPSIS;
      }
   }

   /**
    * Returns the first occurance of an invalid character in the qualifier
    *
    * @param   qualifier   The qualifier to check
    * @return  The invalid char
    */
   private char invalidCharInQualifier(String qualifier)
   {
      if(qualifier.equals("-"))
      {
         return '-';
      }
      else
      {
         return qualifier.replace("-", "").replace("v", "").replace("h", "").charAt(0);
      }
   }
}

