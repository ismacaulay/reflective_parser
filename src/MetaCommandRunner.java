package com.imacaulay;

import com.imacaulay.MetaCommandDefs.MetaCommand;

/**
 * Used to run the different meta commands. It also keeps track of
 * the current state of verbose and quit.
 */
public class MetaCommandRunner
{
   boolean quit_;
   JarObject jarObject_;
   boolean verbose_;

   public MetaCommandRunner(JarObject jarObject, boolean verbose)
   {
      quit_ = false;
      jarObject_ = jarObject;
      verbose_ = verbose;
   }
   
   /**
    * Performs the appropriate actions based on the given MetaCommand
    *
    * @param   command  The command that needs to be executed
    */
   public void execute(MetaCommand command)
   {
      switch(command)
      {
         case QUIT:
         {
            quit_ = true;
            break;
         }
         case VERBOSE:
         {
            verbose_ = !verbose_;
            System.out.println("Verbose " + (verbose_ ? "on" : "off"));
            break;
         }
         case LIST_METHODS:
         {
            jarObject_.printMethods();
            break;
         }
         case PRINT_HELP:
         {
            System.out.println(OutputDefs.INTERPRETER_HELP);
            break;
         }
      }
   }

   public boolean quit()
   {
      return quit_;
   }

   public boolean verbose()
   {
      return verbose_;
   }
}

