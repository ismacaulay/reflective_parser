package com.imacaulay;

import com.imacaulay.FatalErrorException.ErrorCode;
import java.util.ArrayList;

/**
 * Handles parsing the command line arguments
 */
public class CommandLineParser
{
   private boolean verbose_;
   private String jarFile_;
   private String className_;

   public CommandLineParser()
   {
      verbose_ = false;
      jarFile_ = "";
      className_ = "Commands";
   }

   /**
    * Parses the given command line arguments
    *
    * @param args The args to parse
    * @throws FatalErrorException Thrown if any of the arguments causes a fatal error
    */
   public void parseArgs(String[] args) throws FatalErrorException
   {
      // no args given
      if(args.length == 0)
      {
         System.out.println(OutputDefs.SYNOPSIS);
         System.exit(0);
      }

      // get the qualifiers, they come first
      ArrayList<String> qualifiers = new ArrayList<String>();
      for(String arg : args)
      {
         if(!arg.startsWith("-"))
         {
            // end of qualifiers
            break;
         }

         qualifiers.add(arg);
      }

      // check the qualifiers
      for(String qualifier : qualifiers)
      {
         if(isHelpQualifier(qualifier))
         {
            // there are extra qualifiers or command line args with help
            if(qualifiers.size() > 1 || args.length > 1)
            {
               throw new FatalErrorException(ErrorCode.HELP_HAS_EXTRA_QUALIFIERS);
            }

            // output synopsis
            System.out.println(OutputDefs.SYNOPSIS);
            System.exit(0);
         }
         else if(isVerboseQualifier(qualifier))
         {
            verbose_ = true;
         }
      }

      // check remaining args, should be 1 or 2
      int numArguments = (args.length - qualifiers.size());
      if(numArguments > 2)
      {
         throw new FatalErrorException(ErrorCode.TOO_MANY_ARGUMENTS);
      }

      // check jar file
      jarFile_ = args[qualifiers.size()];
      if(!jarFile_.endsWith(".jar"))
      {
         throw new FatalErrorException(ErrorCode.JAR_FILE_NOT_FIRST_ARG);
      }

      // set class
      if(numArguments == 2)
      {
         className_ = args[qualifiers.size()+1];
      }
   }

   public String getJarFile()
   {
      return jarFile_;
   }

   public String getClassName()
   {
      return className_;
   }

   public boolean getVerbose()
   {
      return verbose_;
   }

   private boolean isHelpQualifier(String qualifier) throws FatalErrorException
   {
      boolean isHelp = qualifier.equalsIgnoreCase("--help")
                       || qualifier.equalsIgnoreCase("--h")
                       || qualifier.equals("-h")
                       || qualifier.equals("-?");

      return isValidQualifier(qualifier) && isHelp;
   }

   private boolean isVerboseQualifier(String qualifier) throws FatalErrorException
   {
      boolean isVerbose = qualifier.equalsIgnoreCase("--verbose")
                          || qualifier.equalsIgnoreCase("--v")
                          || qualifier.equals("-v");

      return isValidQualifier(qualifier) && isVerbose;
   }

   private boolean isValidQualifier(String qualifier) throws FatalErrorException
   {
      if(qualifier.startsWith("--"))
      {
         // check long qualifiers
         if(qualifier.equalsIgnoreCase("--help") 
            || qualifier.equalsIgnoreCase("--verbose")
            || qualifier.equalsIgnoreCase("--h")
            || qualifier.equalsIgnoreCase("--v"))
         {
            return true;
         }

         throw new FatalErrorException(ErrorCode.UNRECOGNIZED_LONG_QUALIFIER, qualifier);
      }
      else
      {
         // check short qualifiers
         if((qualifier.contains("?") || qualifier.contains("h")) && qualifier.length() > 2)
         {
            throw new FatalErrorException(ErrorCode.HELP_HAS_EXTRA_QUALIFIERS);
         }

         // check if its one of the only three valid qualifiers
         if(qualifier.equals("-v") || qualifier.equals("-h") || qualifier.equals("-?"))
         {
            return true;
         }
         
         throw new FatalErrorException(ErrorCode.UNRECOGNIZED_QUALIFIER, qualifier);
      }
   }
}

