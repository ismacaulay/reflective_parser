package com.imacaulay;

/**
 * A node that holds either an Integer, Float, or String value
 */
public class ValueNode extends Node
{
   Object value_;

   public ValueNode(Object value)
   {
      super(0);
      value_ = value;
   }

   public Object value()
   {
      return value_;
   }

   public boolean isInteger()
   {
      return (value_ instanceof Integer);
   }

   public boolean isFloat()
   {
      return (value_ instanceof Float);
   }

   public boolean isString()
   {
      return (value_ instanceof String);
   }

   public String toString()
   {
      return value_.toString();
   }
}

