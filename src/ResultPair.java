package com.imacaulay;

/**
 * A pair that contains a token list (String) and a Node object
 * Used when parsing the parse tree to return the a node and the
 * remaining token string. A parameterized pair is not needed here
 * since a pair is only used for Tokens and Nodes.
 */
public class ResultPair
{
   public String tokens;
   public Node node;

   public ResultPair(String tokens, Node node)
   {
      this.tokens = tokens;
      this.node = node;
   }
}
   
