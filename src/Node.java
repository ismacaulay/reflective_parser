package com.imacaulay;

/**
 * Abstract class that represents a Node object which contains an offset to 
 * where the node exists in the expression input. Subclasses must reimplement
 * toString to be able to pretty print the Node.
 */
public abstract class Node
{
   private int offset_;

   public Node(int offset)
   {
      offset_ = offset;
   }
   
   public int offset()
   {
      return offset_;
   }

   public abstract String toString();
}

