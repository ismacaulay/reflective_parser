package com.imacaulay;

import com.imacaulay.MetaCommandDefs.MetaCommand;

import java.util.Scanner;
import java.text.ParseException;

/**
 * The Interpreter gets user input and processes it
 */
public class Interpreter
{
   public Interpreter()
   {
   }

   public void run(JarObject jarObject, boolean verbose)
   {
      Scanner scanner = new Scanner(System.in);

      ExpressionParser expressionParser = new ExpressionParser();
      ExpressionRunner expressionRunner = new ExpressionRunner(jarObject);
      MetaCommandParser metaCommandParser = new MetaCommandParser();
      MetaCommandRunner metaCommandRunner = new MetaCommandRunner(jarObject, verbose);

      // print initial help
      System.out.println(OutputDefs.INTERPRETER_HELP);
      while(!metaCommandRunner.quit())
      {
         // get input
         System.out.print("> ");
         String input = scanner.nextLine();

         try
         {
            MetaCommand metaCommand = MetaCommand.NONE;
            ParseTree parseTree = null;

            // check for meta command, and execute if necessary
            if((metaCommand = metaCommandParser.parseInput(input)) != MetaCommand.NONE)
            {
               metaCommandRunner.execute(metaCommand);
            }
            // check if syntatically correct expression, and execute if necessary
            else if((parseTree = expressionParser.parseInput(input)) != null)
            {
               expressionRunner.execute(parseTree);
            }
         }
         catch(ParseException e)
         {
            System.out.println(e.getMessage() + " at offset " + e.getErrorOffset());
            System.out.println(input);
            for(int i = 0; i < e.getErrorOffset(); i++)
            {
               System.out.print("-");
            }
            System.out.println("^");

            if(metaCommandRunner.verbose())
            {
               e.printStackTrace();
            }
         }
      }
      System.out.println("bye.");
   }
}

