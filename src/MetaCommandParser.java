package com.imacaulay;

import com.imacaulay.MetaCommandDefs.MetaCommand;

/**
 * Parses input strings and checks for meta commands
 */
public class MetaCommandParser
{
   public MetaCommandParser()
   {
   }

   /**
    * Checks if the given input string matches any meta commands
    *
    * @param input The String to check
    * @return The MetaCommand that the input matches, NONE otherwise
    */
   public MetaCommand parseInput(String input)
   {
      // check if the input string matches one of the meta commands
      switch(input)
      {
         case "q":
         {
            return MetaCommand.QUIT;
         }
         case "v":
         {
            return MetaCommand.VERBOSE;
         }
         case "f":
         {
            return MetaCommand.LIST_METHODS;
         }
         case "?":
         {
            return MetaCommand.PRINT_HELP;
         }
      }
      
      return MetaCommand.NONE;
   }
}

