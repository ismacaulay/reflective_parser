package com.imacaulay;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.text.ParseException;
import java.lang.reflect.InvocationTargetException;

/**
 * A class that represents a Jar file and has the ability to invoke methods
 */
public class JarObject
{
   private Class jarClass_;

   public JarObject(Class jarClass)
   {
      jarClass_ = jarClass;
   }

   /**
    * Prints the methods contained in the jar file
    */
   public void printMethods()
   {
      Method[] methods = jarClass_.getDeclaredMethods();
      for(Method m : methods)
      {
         boolean skip = false;

         // only look at public static methods
         if(Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers()))
         {
            String name = m.getName();
            String returnType = stringForClass(m.getReturnType());
            if(returnType == "")
            {
               continue;
            }
            
            String parameters = "";
            Class[] params = m.getParameterTypes();
            for(Class p : params)
            {
               String parameter = stringForClass(p);
               if(parameter == "")
               {
                  skip = true;
                  break;
               }

               parameters += " " + parameter;
            }
            
            if(!skip)
            {
               System.out.println("(" + name + parameters + ") : " + returnType);
            }
         }
      }
   }

   /**
    * Invokes the specified method with the given parameters and returns a Node object containing the value
    *
    * @param methodName The method name to call
    * @param params A list of parameters to use with the method
    * @return A node containing the result of the method
    * @throws ParseException Throws if the method signature does not match the given values
    */
   public Node invokeMethod(ExpressionNode method, ArrayList<ValueNode> params) throws ParseException
   {
      String methodName = method.methodName();
  
      Method[] methods = jarClass_.getDeclaredMethods();

      // find the right method
      for(Method m : methods)
      {
         // only look at public static methods
         if(isPublicStatic(m))
         {
            // found a matching method name
            if(m.getName().equals(methodName))
            {
               Class[] methodParams = m.getParameterTypes();
               // check the paramters
               if(methodParams.length == params.size())
               {
                  boolean foundMethod = true;
                  for(int i = 0; i < params.size(); i++)
                  {
                     ValueNode param = params.get(i);
                     Class methodParam = methodParams[i];
                     if(param.isInteger())
                     {
                        if(!(methodParam == Integer.class) && !(methodParam == int.class))
                        {
                           foundMethod = false;
                           break;
                        }
                     }
                     else if(param.isFloat())
                     {
                        if(!(methodParam == Float.class) && !(methodParam == float.class))
                        {
                           foundMethod = false;
                           break;
                        }
                     }
                     else if(param.isString())
                     {
                        if(!(methodParam == String.class))
                        {
                           foundMethod = false;
                           break;
                        }
                     }
                  }

                  // if we find the method, invoke it with the paramters
                  if(foundMethod)
                  {
                     Object[] arguments = new Object[methodParams.length];
                     for(int i = 0; i < params.size(); i++)
                     {
                        arguments[i] = params.get(i).value();
                     }
                     
                     try
                     {
                        Object result = m.invoke(null, arguments);

                        return new ValueNode(result);
                     }
                     catch(IllegalAccessException e)
                     {
                        throw new ParseException("An Illegal Access occured", method.offset());
                     }
                     catch(InvocationTargetException e)
                     {
                        throw new ParseException("An exception was thrown by the method '" + methodName + "'", method.offset());
                     }
                  }
               }
            }
         }
      }

      String unknownMethod = "(" + methodName;
      for(ValueNode param : params)
      {
         if(param.isFloat())
         {
            unknownMethod += " float";
         }
         else if(param.isInteger())
         {
            unknownMethod += " int";
         }
         else
         {
            unknownMethod += " " + param.value().getClass().getName();
         }
      }
      unknownMethod += ")";

      throw new ParseException("Matching function for '" + unknownMethod + "' not found", method.offset());
   }

   private boolean isPublicStatic(Method method)
   {
      return (Modifier.isPublic(method.getModifiers()) && Modifier.isStatic(method.getModifiers()));
   }

   private String stringForClass(Class c)
   {
      String s = "";
      if(c == Integer.class || c == int.class)
      {
         s = "int";
      }
      else if(c == Float.class || c == float.class)
      {
         s = "float";
      }
      else if(c == String.class)
      {
         s = "string";
      }

      return s;
   }
}

