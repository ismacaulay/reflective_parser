package com.imacaulay;

import java.text.ParseException;

/**
 * Parses the given user input to determine if it is a valid expression
 * and builds a Parse Tree.
 */
public class ExpressionParser
{
   private int offset_;

   public ExpressionParser()
   {
      offset_ = 0;
   }

   /**
    * Parses the given input and returns the ParseTree associated with the input
    *
    * @param input The user input to be parsed
    * @return The parse tree associated with the input
    * @throws ParseException Inidicates that the input is not syntatically correct
    */
   public ParseTree parseInput(String input) throws ParseException
   {
      offset_ = 0;
      
      String tokens = input;
      ResultPair pair = readExpression(tokens);

      // If there are extra tokens, then the parser ended early and it is an error.
      if(!pair.tokens.isEmpty())
      {
         throw new ParseException("Unexpected character encountered", offset_);
      }

      return new ParseTree(pair.node);
   }

   /**
    * Expressions start with (, +, -, ", or a digit
    */
   private ResultPair readExpression(String tokens) throws ParseException
   {
      tokens = stripWhiteSpace(tokens);

      if(tokens.isEmpty())
      {
         return new ResultPair(tokens, null);
      }
      
      // if it starts with (, then its a function
      if(tokens.startsWith("("))
      {
         return readFunction(tokens);
      }
      // if it starts with +, -, ", or a digit, then its a value
      else if(tokens.startsWith("+")
              || tokens.startsWith("-")
              || tokens.startsWith("\"")
              || Character.isDigit(tokens.charAt(0)))
      {
         return readValue(tokens);
      }
      
      // unknown character
      throw new ParseException("Unexpected character encountered", offset_);
   }

   /**
    * Functions start with ( and end with )
    */
   private ResultPair readFunction(String tokens) throws ParseException
   {
      tokens = stripWhiteSpace(tokens);
      
      if(tokens.startsWith("("))
      {
         tokens = removeToken(tokens);
         
         // get the identifier   
         ResultPair pair = readIdentifier(tokens);
         tokens = pair.tokens;
         
         tokens = stripWhiteSpace(tokens);
         
         ExpressionNode parent = (ExpressionNode)pair.node;

         // after the identifier there will be other expressions, so read them until )
         while(!tokens.startsWith(")"))
         {
            ResultPair childPair = readExpression(tokens);
            
            tokens = childPair.tokens;
            parent.addChild(childPair.node);

            tokens = stripWhiteSpace(tokens);

            // if there are no more tokens left, then there is a missing )
            if(tokens.isEmpty())
            {
               throw new ParseException("Encountered end-of-input while reading expression", offset_);
            }
         }
         tokens = removeToken(tokens);

         return new ResultPair(tokens, parent);
      }

      throw new ParseException("Unexpected character encountered", offset_);
   }

   /**
    * Values start with +, -, ", or a digit
    */
   private ResultPair readValue(String tokens) throws ParseException
   {
      tokens = stripWhiteSpace(tokens);

      if(tokens.startsWith("+")
         || tokens.startsWith("-")
         || Character.isDigit(tokens.charAt(0)))
      {
         return readNumber(tokens);
      }
      else if(tokens.startsWith("\""))
      {
         return readString(tokens);
      }

      throw new ParseException("Unexpected character encountered", offset_);
   }

   /**
    * Identifiers must start with [a-zA-Z] followed by [a-zA-Z0-9_]
    */
   private ResultPair readIdentifier(String tokens) throws ParseException
   {
      tokens = stripWhiteSpace(tokens);
      if(!tokens.isEmpty())
      {
         char token = tokens.charAt(0);
         if(isAlpha(token))
         {
            int index = 1;
            offset_++;
            while(index < tokens.length())
            {
               token = tokens.charAt(index);
               if(!isAlphaNum(token))
               {
                  break;
               }

               index++;
               offset_++;
            }
            
            String id = tokens.substring(0, index);

            return new ResultPair(tokens.substring(index, tokens.length()), 
                                      new ExpressionNode(id, offset_-index));
         }
      }
      
      throw new ParseException("Unexcepted character encountered", offset_);
   }

   /**
    * Strings must start and end with a "
    */
   private ResultPair readString(String tokens) throws ParseException
   {
      tokens = stripWhiteSpace(tokens);
      if(!tokens.isEmpty())
      {
         if(tokens.startsWith("\""))
         {
            tokens = removeToken(tokens);
            int index = 0;
            while(index < tokens.length())
            {
               char token = tokens.charAt(index);
               if(token == '"')
               {
                  String s = tokens.substring(0, index);
                  
                  offset_++;
                  index++;
                  return new ResultPair(tokens.substring(index, tokens.length()), 
                                            new ValueNode(s));
               }

               index++;
               offset_++;
            }
         }
         else
         {
            throw new ParseException("Unexpected character encountered", offset_);
         }
      }
      
      throw new ParseException("Encountered end-of-input while reading string", offset_);
   }

   /**
    * Numbers start with +, -, or a digit. They can be an int (Integer) or
    * a float (Float) and must fit within the bounds of the type
    */
   private ResultPair readNumber(String tokens) throws ParseException
   {
      tokens = stripWhiteSpace(tokens);
      if(!tokens.isEmpty())
      {
         int index = 0;
         if(tokens.startsWith("-"))
         {
            index++;
            offset_++;
         }
         else if(tokens.startsWith("+"))
         {
            tokens = removeToken(tokens);
         }

         boolean isFloat = false;
         while(index < tokens.length())
         {
            char token = tokens.charAt(index);
            if(token == '.')
            {
               isFloat = true;
            }
            else if(Character.isLetter(token))
            {
               throw new ParseException("Unexpected character encountered", offset_);
            }
            else if(!Character.isDigit(token))
            {
               break;
            }

            index++;
            offset_++;
         }
         
         String number = tokens.substring(0, index);

         Node node;
         if(isFloat)
         {
            try
            {
               Float f = new Float(number);
               if(f.isInfinite() || f.isNaN())
               {
                  throw new NumberFormatException();
               }

               node = new ValueNode(new Float(number));
            }
            catch(NumberFormatException e)
            {
               throw new ParseException("The float value given in not within the valid range of a float", offset_-number.length());
            }
         }
         else
         {
            try
            {
               node = new ValueNode(new Integer(number));
            }
            catch(NumberFormatException e)
            {
               throw new ParseException("The integer given is not within the valid range of an integer", offset_-number.length());
            }
         }

         return new ResultPair(tokens.substring(index, tokens.length()),
                                   node);
      }
      
      throw new ParseException("Unexpected character encountered", offset_);
   }
   
   private String stripWhiteSpace(String tokens)
   {
      while(tokens.startsWith(" ") || tokens.startsWith("\t"))
      {
         tokens = removeToken(tokens);
      }

      return tokens;
   }

   private String removeToken(String tokens)
   {
      if(!tokens.isEmpty())
      {
         tokens = tokens.substring(1, tokens.length());
         offset_++;
      }
      return tokens;
   }

   private boolean isAlpha(char c)
   {
      c = Character.toLowerCase(c);
      return (('a' <= c && c <= 'z') || c == '_');
   }
   
   private boolean isAlphaNum(char c)
   {
      return (Character.isDigit(c) || isAlpha(c));
   }
}

