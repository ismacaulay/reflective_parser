import java.util.ArrayList;

public class Commands {

	public Commands() {
	}
	
   public static String _hello()
   {
      return "Hello";
   }

   public static int add7(int one, int two, int three, int four, int five, int six, int seven)
   {
      return one + two + three + four + five + six + seven;
   }

   public static float pi() {
      return 3.1415936f;
   }

   public static String hello() {
      return "Hello";
   }

   public static Float pi2() {
      return new Float(3.1415936);
   }

   public static int one() {
      return 1;
   }

   public static Integer two() {
      return new Integer(2);
   }

   public static ArrayList<String> invalid() {
      return new ArrayList<String>();
   }

	public static int add(int x, int y) {
		return x+y;
	}

	public static int mul(int x, int y) {
		return x*y;
	}

	public static int sub(int x, int y) {
		return x-y;
	}

	public static int div(int x, int y) {
		return x/y;
	}

	public static Integer inc(Integer x) {
		return x+1;
	}

	public static Integer dec(Integer x) {
		return x-1;
	}
	
	public static float add(float x, float y) {
		return x+y;
	}

	public static float mul(float x, float y) {
		return x*y;
	}

	public static float sub(float x, float y) {
		return x-y;
	}

	public static float div(float x, float y) {
		return x/y;
	}

	public static Float inc(Float x) {
		return x+1;
	}

	public static Float dec(Float x) {
		return x-1;
	}
	
	public static String add(String x, String y) {
		return x+y;
	}
	
	public static int len(String x) {
		return x.length();
	}
	
}
