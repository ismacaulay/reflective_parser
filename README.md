##CPSC 449 - Java Assignment
###Dependencies
1. ant
2. jdk-7

###Building
In the top level directory run:

```ant```

This will create a jar file called *methods.jar* in *build/jar*

###Running
`java -jar build/jar/methods.jar <arguments>`


